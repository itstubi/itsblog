---
slug: modemnedirnedeğildir
title: Modem Nedir Ne Değildir?
author: Ebutuğrab Kızmaz
author_title: Uykulu
author_url: https://gitlab.com/itstubi
author_image_url: https://cdn.discordapp.com/avatars/337322444157222914/447ffdcdeee1b22baa1524d72a78b46d.webp
tags: [modem, internet, ağ cihazları]
---

# Modem Nedir Ne Değildir?

Modem, ses, radyo veya ışık gibi sinyalleri dijital sinyallere ve tam tersine dönüştürerek internete bağlanmamızı sağlayan bir cihazdır. Adını İngilizce kelimeler olan *Modulation-Demodulation*'dan alır.

<!-- truncate -->

Modemler, internet bağlantısını sağlayan temel cihazlardır ve farklı türleri vardır. En yaygın bağlantı türleri:

⦁ ADSL (Asymmetric Digital Subscriber Line)
⦁ VDSL (Very-high-bit-rate Digital Subscriber Line)
⦁ Fiber Bağlantı

# Modemin İşlevi

İnternet servis sağlayıcılarından aldığımız sinyaller, genellikle elektronik cihazlarımızın anlayabileceği şekilde değildir.

⦁ ADSL ve VDSL: Bu tür bağlantılarda sinyaller analog olarak gelir ve modem tarafından dijital sinyale dönüştürülür.
⦁ Fiber Bağlantı: Fiber bağlantılarda veri, ışık sinyalleri şeklinde iletilir. Bu sinyaller de modem tarafından dijital hale getirilir.

# Modem ve Router Farkı

Evlerimize satın aldığımız ve ya İnternet Servis Sağlayacı(ISS)'larımızın verdiği cihazlar genellikle hem modem hem de router işlevini birleştirir. Ancak bu iki cihazın görevleri farklıdır:

⦁ Modem: İnternet servis sağlayıcısından gelen sinyali dijital sinyale çevirir.
⦁ Router: Ağdaki cihazları birbiriyle ve internetle bağlar. Router’lar genellikle birden fazla LAN portuna ve bir WiFi ağına sahiptir.

Bir router, veri paketlerini cihazlara yönlendirme işini yapar. Ancak WiFi özelliği, routerdan ziyade bir erişim noktası (access point) özelliğidir.

# Son Olarak

Modemler yalnızca sinyal dönüşümünü sağlar. Bunun dışındaki görevler (WiFi ağı oluşturmak, cihazlar arasında veri yönlendirmek gibi) router veya diğer cihazların işlevleridir. Modem üreticileri ise ev kullanıcıların ihtiyaçlarını tek bir cihazda gidermek için bu cihazları birleştirmişler. 

Benim evdeki ağımda bir modem ve bir router bulunuyor.  Routerın WiFi ağı modemin WiFi ağından daha hızlı.  Haliyle diğerini devre dışı bırakıyorum.  Modemdeki 300Mbps hız şuanki aileler içi yavaş kalıyor.  Evinde sunucu barındıranlar içinse eziyet.