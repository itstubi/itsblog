/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'itsblog',
  tagline: 'The tagline of my site',
  url: 'https://itstubi.gitlab.io',
  baseUrl: '/itsblog/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'ItsTubi', // Usually your GitHub org/user name.
  projectName: 'itsblog', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Ebutuğrab Kızmaz',
      logo: {
        alt: 'My Site Logo',
        src: 'img/logo.svg',
      },
      items: [
        { to: 'blog', label: 'Blog', position: 'left' },
        {
          href: 'https://gitlab.com/itstubi',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'İletişim',
          items: [
            {
              label: 'Linkedin',
              href: 'https://www.linkedin.com/in/ebutugrabkizmaz',
            },
            {
              label: 'Gitlab',
              href: 'https://gitlab.com/itstubi',
            },
            {
              html: 'Email: e.tugrabkizmaz+iletisim@gmail.com',
            },
          ],
        },
      ],
    },
    // algolia: {
    //   contextualSearch: true,
    // },
    metaData: [
      { name: "algolia-site-verification", content: "6C112EC3CE8DB028" }
    ],
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: false,
        blog: {
          showReadingTime: true,
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),

        },
      },
    ],
  ],
};
